package cs102;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;

public class JOperandButton extends JButton {
    public JOperandButton(String text) {
        super(text);
        this.setBackground(Color.WHITE);
        this.setFont(new Font("Courier", Font.PLAIN, 20));
    }
}
