package cs102;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;

public class JOperatorButton extends JButton {
    public JOperatorButton(String text) {
        super(text);
        this.setBackground(Color.GRAY);
        this.setFont(new Font("Courier", Font.PLAIN, 20));
    }
}
