package cs102;

import javax.swing.*;
import java.awt.*;

public class Main {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Calculator");
        frame.setLayout(new BorderLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 300);

        JPanel topPanel = new JPanel();
        frame.add(topPanel, BorderLayout.NORTH);
        topPanel.setBackground(Color.WHITE);
        topPanel.setLayout(new BorderLayout());

        JLabel label = new JLabel("0");
        label.setFont(new Font("Courier", Font.PLAIN, 20));
        topPanel.add(label, BorderLayout.EAST);

        JPanel mainPanel = new JPanel();
        frame.add(mainPanel, BorderLayout.CENTER);

        mainPanel.setLayout(new GridLayout(4, 4));
        mainPanel.add(new JOperandButton("7"));
        mainPanel.add(new JOperandButton("8"));
        mainPanel.add(new JOperandButton("9"));
        mainPanel.add(new JOperatorButton("/"));
        mainPanel.add(new JOperandButton("4"));
        mainPanel.add(new JOperandButton("5"));
        mainPanel.add(new JOperandButton("6"));
        mainPanel.add(new JOperatorButton("x"));
        mainPanel.add(new JOperandButton("1"));
        mainPanel.add(new JOperandButton("2"));
        mainPanel.add(new JOperandButton("3"));
        mainPanel.add(new JOperatorButton("-"));
        mainPanel.add(new JOperandButton("0"));
        mainPanel.add(new JOperandButton("."));
        mainPanel.add(new JOperatorButton("="));
        mainPanel.add(new JOperatorButton("+"));

        frame.setVisible(true);
    }
}
